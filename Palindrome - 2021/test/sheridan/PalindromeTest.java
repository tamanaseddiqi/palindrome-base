package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindropmeRegular( ) {
		boolean test = Palindrome.isPalindrome("racecar");
		assertTrue("Value provided is invalid", test);
	}

	@Test
	public void testIsPalindromeException( ) {
		boolean test = Palindrome.isPalindrome("fail");
		assertFalse("Value provided is invalid", test);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean test = Palindrome.isPalindrome("Tcao  act");
		assertTrue("Value provided is invalid", test);	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean test = Palindrome.isPalindrome("race on car");
		assertFalse("Value provided is invalid", test);
	}

}
